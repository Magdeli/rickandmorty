Magdeli Holmøy Asplin
9/25/2019

This webpage displays a list of Rick and Morty characters with information about each, and it lets you click on a character to view only one.
It also has a navbar that lets you head into a page of Rick and Morty locations, and lets you click on each location to view only one.
