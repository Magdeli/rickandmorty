import React, { useState, useEffect } from "react";
import { NavLink } from "react-router-dom";

const LocationCard = props => {
	//const [residents, setResidents] = useState([]);

	const link = props.showLink ? (
		<NavLink property="/" className="nav-link" to={"/location/" + props.id}>
			View this location
		</NavLink>
	) : null;

	// useEffect(() => {
	// 	const ids = props.residents.map(resident => resident.split("/").slice(-1)[0]);

	// 	fetch(`https://rickandmortyapi.com/api/character/${ids}`)
	// 		.then(res => res.json())
	// 		.then(data => {
	// 			if (data.error) {
	// 				throw new Error(data.error);
	// 			}

	// 			if (Array.isArray(data.results)) {
	// 				setResidents([...data.results]);
	// 			} else {
	// 				setResidents([data]);
	// 			}
	// 		})
	// 		.catch(error => {
	// 			console.log(error);
	// 		});
	// }, [props.residents]);

	//if (residents.length > 0) {
	return (
		<div
			className="col-xs-12 col-sm-6 col-md-4 text-dark mb-4"
			style={{ margin: props.centered ? "0 auto" : "0" }}
		>
			<div className="card CharacterCard">
				<div className="card-body">
					<h5 className="card-title">{props.name}</h5>
					<b>Type: </b> {props.type} <br />
					<b>Dimension: </b> {props.dimension} <br />
					{/* <b>Residents: </b>
						{residents.map(r => (
							<p key={r.id}> {r.name}</p>
						))} */}
					<br />
					<b>{link}</b>
				</div>
			</div>
		</div>
	);
	//} else {
	// 	return <div></div>;
	// }
};

export default LocationCard;
