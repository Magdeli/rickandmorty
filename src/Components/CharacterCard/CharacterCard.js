import React from "react";
import { NavLink } from "react-router-dom";

//import "bootstrap";
//import "jquery";
//import "popper";

const characterCard = props => {
	const link = props.showLink ? (
		<NavLink property="/" className="nav-link" to={"/character/" + props.id}>
			View this character
		</NavLink>
	) : null;

	return (
		<div
			className="col-xs-12 col-sm-6 col-md-4 text-dark mb-4"
			style={{ margin: props.centered ? "0 auto" : "0" }}
		>
			<div className="card CharacterCard">
				<img src={props.image} alt={props.name} className="card-img-top" />

				<div className="card-body">
					<h5 className="card-title">{props.name}</h5>
					<b>Species: </b> {props.species} <br />
					<b>Status: </b> {props.status} <br />
					<b>Gender: </b> {props.gender} <br />
					<b>Location: </b> {props.location} <br />
					<b>Place of origin: </b> {props.origin} <br />
					<b>{link}</b>
				</div>
			</div>
		</div>
	);
};

export default characterCard;
