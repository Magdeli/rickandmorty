import React, { Component } from "react";

class SearchCharacter extends Component {
	constructor(props) {
		super(props);
		this.state = {
			input: "",
			rickMorty: []
		};
		this.handleCharacterSearch = this.handleCharacterSearch.bind(this);
	}

	handleCharacterSearch(e) {
		const input = e.target.value;

		this.setState({ input });
	}

	componentDidUpdate() {
		if (this.props.onChange) {
			this.props.onChange(this.state);
		}
	}

	render() {
		return (
			<div>
				<div className="form-group">
					<label>Name of Character:</label> <br />
					<input type="text" onChange={this.handleCharacterSearch} />
					<button type="button" onClick={() => this.props.onSearchButtonClick(this.state.input)}>
						Search
					</button>
				</div>
			</div>
		);
	}
}

export default SearchCharacter;
