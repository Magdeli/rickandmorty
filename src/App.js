import React from "react";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { Nav, Navbar } from "react-bootstrap";

function App(props) {
	return (
		<>
			<Navbar bg="light" expand="lg">
				<Navbar.Brand href="/">Rick and Morty Characters</Navbar.Brand>
				<Navbar.Toggle aria-controls="basic-navbar-nav" />
				<Navbar.Collapse id="basic-navbar-nav">
					<Nav className="mr-auto">
						<Nav.Link href="/">Characters</Nav.Link>
						<Nav.Link href="/location">Locations</Nav.Link>
					</Nav>
				</Navbar.Collapse>
			</Navbar>

			<div className="App bg-dark text-white">
				<div className="CardsPage">
					<div>{props.children}</div>
				</div>
			</div>
		</>
	);
}

export default App;
