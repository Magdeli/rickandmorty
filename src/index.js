import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import { BrowserRouter, Route } from "react-router-dom";
import CardPage from "./Containers/CharacterCards/CardPage.js";
import CardsPage from "./Containers/CharacterCards/CardsPage.js";
import LocationCardsPage from "./Containers/LocationCardsPage.js";
import LocationCardPage from "./Containers/LocationCardPage.js";

ReactDOM.render(
	<BrowserRouter>
		<App>
			<Route path="/character/:id" component={CardPage} />
			<Route exact path="/" component={CardsPage} />
			<Route exact path="/location" component={LocationCardsPage} />
			<Route path="/location/:id" component={LocationCardPage} />
		</App>
	</BrowserRouter>,
	document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
