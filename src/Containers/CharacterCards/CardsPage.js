import React from "react";
import CharacterCard from "./../../Components/CharacterCard/CharacterCard.js";
import SearchCharacter from "../../Components/SearchCharacter/SearchCharacter.js";

class CardsPage extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			rickMorty: [],
			characterCards: []
		};

		this.getCharacters = this.getCharacters.bind(this);
		this.handleSearchButtonOnClick = this.handleSearchButtonOnClick.bind(this);
	}

	getCharacters(page = 1) {
		fetch(`https://rickandmortyapi.com/api/character/?page=${page}`)
			.then(res => res.json())
			.then(data => {
				console.log(data);
				if (data.error) {
					throw new Error(data.error);
				}
				this.setState(previousState => ({
					rickMorty: [...previousState.rickMorty, ...data.results]
				}));
			})
			.catch(error => {
				console.log(error);
			});
	}

	componentDidMount() {
		this.getCharacters();
	}

	handleSearchButtonOnClick(input) {
		fetch(`https://rickandmortyapi.com/api/character/?name=${input}`)
			.then(res => res.json())
			.then(data => {
				console.log(data);
				if (data.error) {
					throw new Error(data.error);
				}
				this.setState(previousState => ({
					rickMorty: [...data.results]
				}));
			})
			.catch(error => {
				console.log(error);
			});
	}

	render() {
		let characters = null;
		if (this.state.rickMorty.length > 0) {
			characters = this.state.rickMorty.map(character => (
				<CharacterCard
					key={character.id}
					id={character.id}
					image={character.image}
					name={character.name}
					species={character.species}
					status={character.status}
					gender={character.gender}
					location={character.location.name}
					origin={character.origin.name}
					showLink={true}
				></CharacterCard>
			));
		} else {
			characters = <p>Loading...</p>;
		}

		return (
			<React.Fragment>
				<header>
					<h1>Rick and Morty Characters</h1>
				</header>
				<br />
				<SearchCharacter onSearchButtonClick={this.handleSearchButtonOnClick} />
				<br />
				<div className="row w-100">{characters}</div>
			</React.Fragment>
		);
	}
}

export default CardsPage;
