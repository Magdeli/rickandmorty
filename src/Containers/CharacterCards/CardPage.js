import React from "react";
import CharacterCard from "./../../Components/CharacterCard/CharacterCard.js";

class CardPage extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			id: props.match.params.id,
			character: {}
		};

		this.getCharacterData = this.getCharacterData.bind(this);
	}

	getCharacterData() {
		fetch("https://rickandmortyapi.com/api/character/" + this.state.id)
			.then(res => res.json())
			.then(data => {
				console.log(data);
				this.setState({ character: data });
			})
			.catch(error => {
				console.log(error);
			});
	}

	componentDidMount() {
		this.getCharacterData();
	}

	render() {
		let characterCard = null;
		if (this.state.character.id) {
			characterCard = (
				<CharacterCard
					centered={true}
					key={this.state.character.id}
					id={this.state.character.id}
					image={this.state.character.image}
					name={this.state.character.name}
					species={this.state.character.species}
					status={this.state.character.status}
					gender={this.state.character.gender}
					location={this.state.character.location.name}
					origin={this.state.character.origin.name}
				></CharacterCard>
			);
		} else {
			characterCard = <p>Loading...</p>;
		}

		return <React.Fragment>{characterCard}</React.Fragment>;
	}
}

export default CardPage;
