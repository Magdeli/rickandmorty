import React from "react";
import LocationCard from "./../Components/LocationCard.js";

class LocationCardPage extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			id: props.match.params.id,
			location: {}
		};

		this.getLocationData = this.getLocationData.bind(this);
	}

	getLocationData() {
		fetch("https://rickandmortyapi.com/api/location/" + this.state.id)
			.then(res => res.json())
			.then(data => {
				console.log(data);
				this.setState({ location: data });
			})
			.catch(error => {
				console.log(error);
			});
	}

	componentDidMount() {
		this.getLocationData();
	}

	render() {
		let locationCard = null;
		if (this.state.location.id) {
			locationCard = (
				<LocationCard
					centered={true}
					key={this.state.location.id}
					id={this.state.location.id}
					name={this.state.location.name}
					type={this.state.location.type}
					dimension={this.state.location.dimension}
					// residents={this.state.location.residents}
				></LocationCard>
			);
		} else {
			locationCard = <p>Loading...</p>;
		}

		return <React.Fragment>{locationCard}</React.Fragment>;
	}
}

export default LocationCardPage;
