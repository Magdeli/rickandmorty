import React from "react";
import LocationCard from "./../Components/LocationCard.js";

class LocationCardsPage extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			locationCards: [],
			residentID: []
		};

		this.getLocations = this.getLocations.bind(this);
		//this.handleSearchButtonOnClick = this.handleSearchButtonOnClick.bind(this);
	}

	getLocations(page = 1) {
		fetch(`https://rickandmortyapi.com/api/location/?page=${page}`)
			.then(res => res.json())
			.then(data => {
				if (data.error) {
					throw new Error(data.error);
				}
				this.setState(previousState => ({
					locationCards: [...previousState.locationCards, ...data.results]
				}));
			})
			.catch(error => {
				console.log(error);
			});
	}

	componentDidMount() {
		this.getLocations();
	}

	/* handleSearchButtonOnClick(input) {
		fetch(`https://rickandmortyapi.com/api/character/?name=${input}`)
			.then(res => res.json())
			.then(data => {
				console.log(data);
				if (data.error) {
					throw new Error(data.error);
				}
				this.setState(previousState => ({
					rickMorty: [...data.results]
				}));
			})
			.catch(error => {
				console.log(error);
			});
    } */

	render() {
		let location = null;
		if (this.state.locationCards.length > 0) {
			location = this.state.locationCards.map(location => {
				this.state.residentID.forEach(resident => {
					resident = resident.name;
				});

				return (
					<LocationCard
						key={location.id}
						id={location.id}
						name={location.name}
						type={location.type}
						dimension={location.dimension}
						residents={location.residents}
						showLink={true}
					></LocationCard>
				);
			});
		} else {
			location = <p className="text-white">Loading...</p>;
		}

		return (
			<React.Fragment>
				<header>
					<h1>Rick and Morty Locations</h1>
				</header>
				<br />
				<div className="row w-100">{location}</div>
			</React.Fragment>
		);
	}
}

export default LocationCardsPage;
